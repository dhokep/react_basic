import './App.css';
import InputText from './component/InputText';
import Text from './component/Text';
import { useState } from 'react'



function App() {

const [data, setData] = useState('');
  function addText(text){
    console.log(text);   

    setData(text);

  }

  return (
          <>
            <InputText addText={addText}/>            
            <Text data={data}/> 
          </>  
    );
}

export default App;
