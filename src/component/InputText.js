import React from 'react'
import { useState } from 'react'

export default function InputText(props) {
	const [text, setText] = useState('');

	const handleChange = (e) => {
    	setText(e.target.value);
    	//props.data = e.target.value;
  	} 

  	const submit = (e) => {
  		e.preventDefault();
  		if(!text){
  			alert('fields cannot be blank')
  		}
  		props.addText(text);
  		setText('');
  	}

	return (			
				<form onSubmit={submit} >
				 <center><h3>Input Something</h3></center>
	  			 <input type="text" value={text} onChange={handleChange} className="form-control" placeholder="Input Something"/>
	  			 <center><button type="submit" className="btn btn-primary my-3">Submit</button></center>
				</form>

		)
}